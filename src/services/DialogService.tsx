let _dialog;

function setDialogRef(dialogRef) {
    _dialog = dialogRef;
}

function showDialog() {
    _dialog.showDialogComponent();
}
function hideDialog() {
    _dialog.hideDialogComponent();
}

function setDialogProps(props) {
    _dialog.setDialogProps(props);
}

function dialog() {
    return _dialog
}

export default {
    showDialog,
    hideDialog,
    setDialogProps,
    setDialogRef,
    dialog
};