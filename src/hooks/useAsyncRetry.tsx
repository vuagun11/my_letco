import useAsyncRetry from "react-use/lib/useAsyncRetry";

/**
 * @link https://github.com/streamich/react-use/blob/master/docs/useAsyncRetry.md
 */
export default useAsyncRetry;
