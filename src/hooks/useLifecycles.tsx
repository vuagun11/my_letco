import useLifecycles from "react-use/lib/useLifecycles";

/**
 * @link https://github.com/streamich/react-use/blob/master/docs/useLifecycles.md
 */
export default useLifecycles;
