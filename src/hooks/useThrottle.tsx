import useThrottle from "react-use/lib/useThrottle";

/**
 * @link https://github.com/streamich/react-use/blob/master/docs/useThrottle.md
 */
export default useThrottle;
