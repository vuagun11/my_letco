import {useState, useEffect} from 'react';

type setDataType<DataType> = (data: DataType) => any;
type idType = string | number

const eventListeners: Record<string, Record<idType, setDataType>> = {};
const addEventListener = <DataType>(name: string, id: string | number, setData: setDataType<DataType>) => {
    const listeners = eventListeners[name] || {};
    return Object
        .assign(
            eventListeners,
            {
                [name]: {
                    ...listeners,
                    [id]: setData,
                },
            },
        );
};

const removeEventListener = (name, id) => (
    delete eventListeners[name][id]
);

export const useEvent = <DataType>(name: string, withInitialData: DataType) => {
    const [id] = useState(() => Math.random().toString(32));
    const [data, setData] = useState<DataType>(withInitialData);
    useEffect(() => {
        addEventListener(name, id, setData);
        return () => removeEventListener(name, id);
    }, []);
    return data;
};

export const emitEvent = (name, withData) => {
    const listeners = eventListeners[name] || {};
    Object.values(listeners).map(setData => setData(withData));
};