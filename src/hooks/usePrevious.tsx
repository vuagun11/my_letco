import usePrevious from "react-use/lib/usePrevious";

/**
 * @link https://github.com/streamich/react-use/blob/master/docs/usePrevious.md
 */
export default usePrevious;
