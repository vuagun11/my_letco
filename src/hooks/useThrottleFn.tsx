import useThrottleFn from "react-use/lib/useThrottleFn";

/**
 * @link https://github.com/streamich/react-use/blob/master/docs/useThrottle.md
 */
export default useThrottleFn;
