import {useMemo} from "react";
import useToggle from "./useToggle";

/**
 * Return array of [value, setTrue, setFalse], you can call setTrue and setFalse without setup useCallback
 */
const useBoolean = (initialValue: boolean = false): [boolean, () => void, () => void] => {
    const [value, toggle] = useToggle(initialValue);

    const {setTrue, setFalse} = useMemo(() => ({
        setTrue: () => toggle(true),
        setFalse: () => toggle(false)
    }), []);

    return [value, setTrue, setFalse];
};

export default useBoolean;
