import {config, api} from './Config';
import Core from "./Core";
import axios, {AxiosRequestConfig} from 'axios';
import CryptoJS from "react-native-crypto-js";

let headers = {};

let _axios = axios.create({headers});

function show_query_string(url, obj) {
    // @ts-ignore
    if (Core && Core.user && Core.user.access_token) {
        headers = {
            ...headers,
            // @ts-ignore
            'Authorization': 'Bearer ' + Core.user.access_token,
            'Content-Type': 'application/json',
        };
        _axios = axios.create({headers})
    }

    console.log(url + '?' + Object.keys(obj).reduce(function (a, k) {
        // @ts-ignore
        a.push(k + '=' + encodeURIComponent(obj[k]));
        return a
    }, []).join('&'));
    return
}

async function login(username, password) {
    let response: any;
    let requestConfig: any;
    let md5_password = CryptoJS.MD5(password).toString();
    const data = {
        username: username,
        key: CryptoJS.MD5(username + md5_password).toString(),
        password: md5_password,
    };

    try {
        requestConfig = {
            method: 'post',
            url: api.apiUserLogin,
            data: data,
            headers: {}
        };
        response = await _axios(requestConfig);
        return response.data;
    } catch (error) {
        console.log('error login ', error.response);
        return null
    }
}

async function logout(skey) {
    let response: any;
    let requestConfig: any;
    const data = {
        skey: skey
    };

    try {
        requestConfig = {
            method: 'post',
            url: api.apiUserLogout,
            data: data,
            headers: {}
        };
        response = await _axios(requestConfig);
        return response.data;
    } catch (error) {
        console.log('error login ', error.response);
        return null
    }
}


async function checkout(coord, skey) {
    let response: any;
    let requestConfig: any;
    const data = coord;
    data['skey'] = skey;
    console.log(data)
    try {
        requestConfig = {
            method: 'post',
            url: api.apiUserTracking,
            data: data,
            headers: {}
        };
        response = await _axios(requestConfig);
        return response.data;
    } catch (error) {
        console.log('error send ', error.response);
        return null
    }
}

export default {
    login,
    logout,
    checkout
}
