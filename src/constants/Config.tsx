import {Platform} from "react-native";

const config = {
    BACKEND_URL: 'http://me.letco.vn/mobileapi'
    // BACKEND_URL: 'http://192.168.1.7:5000'
};

export const Fonts = {
    SemiBold: Platform.OS === 'ios' ? 'SFUIText-Semibold' : 'SF-UI-Text-Semibold',
    Regular: Platform.OS === 'ios' ? 'SFUIText-Regular' : 'SF-UI-Text-Regular',
    Light: Platform.OS === 'ios' ? 'SFUIText-Light' : 'SF-UI-Text-Light',
};


const menu = {
    vi: {
        gioi_thieu: {
            url: 'https://hochiminhjourney.com/tong-quan/',
            title: 'Giới thiệu '
        },
        huong_dan_su_dung: {
            url: 'https://hochiminhjourney.com/huong-dan-su-dung-app/',
            title: 'Hướng dẫn sử dụng app'
        }
    },
    'en': {
        gioi_thieu: {
            url: 'https://hochiminhjourney.com/tong-quan/',
            title: 'Introduce'
        },
        huong_dan_su_dung: {
            url: 'https://hochiminhjourney.com/en/huong-dan-su-dung-app/',
            title: 'Guide App'
        }
    }
};

const api = {
    apiUserLogin: config.BACKEND_URL + '/login',
    apiUserLogout: config.BACKEND_URL + '/logout',
    apiUserTracking: config.BACKEND_URL + '/tracking'
};

export {
    config,
    api,
    menu
};
