import {AsyncStorage} from "react-native";
import {UPDATE_USER,} from "../reducers/type";
import Core, {updateUser} from "./Core";


export const syncData = async (dispatch: any) => {
    const user = await AsyncStorage.getItem('@user');

    updateUser(user ? JSON.parse(user) : null);
    dispatch({
        type: UPDATE_USER,
        payload: user ? JSON.parse(user) : null
    });
    return user;

};
