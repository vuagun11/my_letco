let Core = {
    locale: 'vi',
    accessToken: '',
    currentPosition: null,
    lastPosition: {
        latitude: 21.0227788,
        longitude: 105.8194541,
        latitudeDelta: 0.15,
        longitudeDelta: 0.15
    },
    user: {},
    constant: {
        tour_categories: [],
        post_categories: [],
        place_services: [],
        place_facilities: [],
        place_categories: [],
        menu_item_categories: [],
    }
};

export function updateLocale(locale: string) {
    Core.locale = locale
}

export function updateCurrentPosition(position: any) {
    Core.currentPosition = {
        ...Core.currentPosition,
        latitudeDelta: 0.15,
        longitudeDelta: 0.15,
        ...position
    }
}
export function updateLastPosition(position: any) {
    Core.lastPosition = {
        ...Core.lastPosition,
        ...position
    }
}


export function updateUser(user: any) {
    Core.user = user
}


export function updateConstant(constant: any) {
    Core.constant = constant
}

export default Core
