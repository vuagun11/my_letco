import React, {memo} from 'react';
import {View, StyleSheet, Text} from 'react-native'

const DetailScreen = memo(() => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>
                DetailScreen
            </Text>
        </View>
    )
});
export default DetailScreen;

const styles = StyleSheet.create({
    containerCenter: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 50,
        color: '#333333'
    }
});
