import React, {memo} from 'react';
import {View, StyleSheet, Text, Image, TextInput} from 'react-native'
import {Button} from "../components/Button";

interface Props {
    store: any,
    dispatch: any
}

interface State {
    loading: boolean
}


const LoginScreen = memo(() => {
    return (
        <View style={styles.container}>
            <View style={[styles.logoBlock]}>
                <Image style={{width: 280, height: 70, resizeMode: 'stretch'}} source={require('../assets/images/logo.png')}/>
            </View>
            <View style={styles.inputTextBlock}>
                <TextInput style={styles.inputTextLogin} placeholder={'Tên đăng nhập'}/>
                <TextInput style={styles.inputTextLogin} placeholder={'Mật khẩu'}/>
            </View>
            <View style={styles.blockLoginBtn}>
                <Button text={'Đăng nhập'}/>
            </View>
        </View>
    );
})
export default LoginScreen;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EEEEEE',
        padding: 20
    },
    blockLoginBtn: {
        marginTop: 10
    },
    blockBottom: {
        marginTop: 20,
        flex: 1,
        flexDirection: 'row'
    },
    btnLogin: {
        borderWidth: 1,
        borderColor: '#3079ed',
        backgroundColor: '#4d90fe',

    },
    logoBlock: {
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center'
    },
    inputTextBlock: {
        marginTop: 40,
    },
    inputTextLogin: {
        height: 60,
        borderRadius: 2,
        borderColor: 'gray',
        borderWidth: 1,
        marginTop: 10,
        fontSize: 20
    },
    containerCenter: {
        alignItems: 'center',
    },
});

