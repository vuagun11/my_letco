import React, {Component, memo, useEffect, useState} from 'react';
import {
    View,
    Image,
    StyleSheet, TextInput, AsyncStorage,
} from 'react-native';
import Endpoint from "../constants/Endpoint";
import {connect, useDispatch} from 'react-redux';
import {Button} from "../components/Button";
import {updateUser} from "../constants/Core";
import {UPDATE_USER} from "../reducers/type";
import {useUser} from "../reducers/hooks";
import {Route} from "../routes";
import {NavigationScreenProps} from "react-navigation";
import {useSelector} from 'react-redux';
import {syncData} from "../constants/Actions";

interface Props extends NavigationScreenProps {
    store: any,
    dispatch: any
}


const LoginScreen = memo((props: Props) => {
    const dispatch = useDispatch();
    const {navigation} = props;
    const [username, setUsername] = useState<string>('');
    const [password, setPassword] = useState<string>('');

    const _sendLogin = async () => {
        let response_user = await Endpoint.login(username, password);
        if (response_user.err === 0) {
            console.log(response_user);
            updateUser(response_user.data);
            dispatch({
                type: UPDATE_USER,
                payload: response_user.data
            });
            navigation.navigate(Route.HomeScreen);
        }
    };

    useEffect(() => {
        (async function sync_data() {
            syncData(props.dispatch).then((buser) => {
                if (buser) navigation.navigate(Route.HomeScreen);
            });
        })();
    }, []); // load lan dau tien khi man hinh dc render (chay 1 lan duy nhat)

    return (
        <View style={styles.container}>
            <View style={[styles.logoBlock]}>
                <Image style={{width: 280, height: 70, resizeMode: 'stretch'}} source={require('../assets/images/logo.png')}/>
            </View>
            <View style={styles.inputTextBlock}>
                <TextInput value={username} onChangeText={(text) => setUsername(text)} style={styles.inputTextLogin}
                           placeholder={'Tên đăng nhập'}/>
                <TextInput secureTextEntry={true} value={password} onChangeText={(text) => setPassword(text)} style={styles.inputTextLogin}
                           placeholder={'Mật khẩu'}/>
            </View>
            <View style={styles.blockLoginBtn}>
                <Button text={'Đăng nhập'} onPress={_sendLogin}/>
            </View>
        </View>
    )
})

const mapStateToProps = (state: any) => ({
    store: state.store
});

export default connect(mapStateToProps)(LoginScreen);
const styles = StyleSheet.create({
    container: {
        padding: 10,
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#EEEEEE'
    },
    blockLoginBtn: {
        marginTop: 10
    },
    blockBottom: {
        marginTop: 20,
        flex: 1,
        flexDirection: 'row'
    },
    btnLogin: {
        borderWidth: 1,
        borderColor: '#3079ed',
        backgroundColor: '#4d90fe',

    },
    logoBlock: {
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center'
    },
    inputTextBlock: {
        marginTop: 40,
    },
    inputTextLogin: {
        height: 60,
        borderRadius: 2,
        borderColor: 'gray',
        borderWidth: 1,
        marginTop: 10,
        fontSize: 20
    },
    containerCenter: {
        alignItems: 'center',
    },
});
