import React, {memo, useEffect, useState} from 'react';
import {View, StyleSheet, Text, Image} from 'react-native'
import {Button} from "../components/Button";
import {NavigationScreenProps} from "react-navigation";
import {Route} from "../routes";
import Geolocation from "@react-native-community/geolocation";
import {useUser} from "../reducers/hooks";
import {syncData} from "../constants/Actions";
import {connect, useDispatch} from 'react-redux';
import Endpoint from "../constants/Endpoint";
import {updateUser} from "../constants/Core";
import {UPDATE_USER} from "../reducers/type";

interface Props extends NavigationScreenProps {
    store: any,
    dispatch: any
}


const HomeScreen = memo((props: Props) => {
    const {navigation} = props;
    const user = useUser();
    const dispatch = useDispatch();


    useEffect(() => {
        (async function sync_data() {
            syncData(props.dispatch).then((buser) => {
                console.log('buser', buser, typeof (buser));
                if (!buser) {
                    console.log('not have user');
                    navigation.navigate(Route.LoginScreen)
                }
            });
        })();
    }, []); // load lan dau tien khi man hinh dc render (chay 1 lan duy nhat)

    const watchFunc = async (position) => {
        let response = await Endpoint.checkout(position.coords, user.skey);
        console.log(response);
        if (response['err'] === 0) {
            console.log('Check out thanh cong')
        }
    };
    const checkOut = async () => {
        Geolocation.watchPosition(watchFunc);

    };

    const _sendLogout = async () => {
        let response = await Endpoint.logout(user.skey);
        if (response.err === 0) {
            updateUser(null);
            dispatch({
                type: UPDATE_USER,
                payload: ''
            });
            navigation.navigate(Route.LoginScreen);
        }

    };

    return (
        <View style={styles.container}>
            <Text style={styles.title}>
                Home Screen
            </Text>
            <View style={styles.containerCenter}>
                <Button
                    onPress={checkOut}
                    containerStyle={styles.btnCheckOut}
                    text={'Check OUT'}
                    textStyle={{
                        color: '#fff'
                    }}
                />
            </View>
            <View style={styles.containerCenter}>
                <Button
                    onPress={_sendLogout}
                    containerStyle={styles.btnCheckOut}
                    text={'Logout'}
                    textStyle={{
                        color: '#fff'
                    }}
                />
            </View>
        </View>
    )
});
const mapStateToProps = (state: any) => ({
    store: state.store
});

export default connect(mapStateToProps)(HomeScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EEEEEE'
    },
    title: {
        fontSize: 30,
        color: '#333333'
    },
    btnCheckOut: {
        width: 120,
        height: 40,
        borderRadius: 4,
        backgroundColor: '#0077cc',
        alignItems: 'center',
        justifyContent: 'center'
    },
    containerCenter: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
