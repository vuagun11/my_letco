import React from 'react';
import {createAppContainer, createStackNavigator} from 'react-navigation';
import HomeScreen from "../screens/HomeScreen";
import DetailScreen from "../screens/DetailScreen";
// import LoginScreen from "../screens/LoginScreen";
import LoginScreen from "../screens/BLoginScreen";
import {Route} from "../routes";

const AppNavigator = createStackNavigator(
    {
        [Route.HomeScreen]: {
            screen: HomeScreen,
            path: Route.HomeScreen,
            navigationOptions: () => ({
                header: null,
            }),
        },
        [Route.DetailScreen]: {
            screen: DetailScreen,
            path: Route.DetailScreen,
            navigationOptions: () => ({
                header: null,
            }),
        },
        [Route.LoginScreen]: {
            screen: LoginScreen,
            path: Route.HomeScreen,
            navigationOptions: () => ({
                header: null,
            }),
        }
    },
    {
        initialRouteName: Route.HomeScreen,
    },
);


export default createAppContainer(AppNavigator);
