import {combineReducers, createStore} from "redux";
import {UPDATE_LOCALE, UPDATE_USER, UPDATE_CURRENT_POSITION} from "./type";
import {AsyncStorage} from "react-native";


const initialState = {
    user: null
};

const reducers = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_USER:
            AsyncStorage.setItem('@user', action.payload ? JSON.stringify(action.payload) : '');
            return {
                ...state,
                user: action.payload
            };
        default:
            return state
    }
};

const rootReducer = combineReducers({
    store: reducers
});

export default createStore(rootReducer);
