import {useSelector} from 'react-redux';

export const useLocale = (equalityFn?: any) => {
    return useSelector(((state: any) => {
        return state.store.locale
    }));
};


export const useUser = () => {
    return useSelector((state: any) => state.store.user)
};
