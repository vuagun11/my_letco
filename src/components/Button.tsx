import React, {memo} from 'react';
import {View, StyleSheet, Text, TextStyle, ViewStyle, TouchableOpacity} from 'react-native';

interface Props {
    containerStyle?: ViewStyle,
    text?: string,
    textStyle?: TextStyle,
    onPress?: () => void
}

export const Button = memo((props: Props) => {
    const {
        containerStyle,
        text,
        textStyle,
        onPress
    } = props;
    return (
        <TouchableOpacity
            onPress={onPress}
            style={[styles.container, containerStyle]}>
            {
                text
                    ? <Text style={[styles.textStyle, textStyle]}>
                        {text}
                    </Text>
                    : null
            }
        </TouchableOpacity>
    )
});
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#4d90fe',
        alignItems: 'center',
        justifyContent: 'center',
        minWidth: 46,
        height: 50,
        borderRadius: 5
    },
    textStyle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: "white",
        textShadowColor: '0 1px rgba(0,0,0,0.1)',


    }
});
