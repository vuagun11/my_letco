import React, {memo} from 'react';
import {View, StyleSheet, Text, TextStyle, ViewStyle, TouchableOpacity} from 'react-native';

interface Props {
    title?: string,
    leftComponent?: any
}

export const Header = memo((props: Props) => {
    const {
        title,
        leftComponent
    } = props;
    return (
        <View style={styles.container}>

        </View>
    )
});
const styles = StyleSheet.create({
    container: {
        height: 60,
        alignItems: 'center',
        justifyContent: 'center'
    },
});
