import React, {forwardRef, useEffect, useState} from 'react';
import {
    Text,
    ViewStyle,
    View,
    StatusBar
} from 'react-native';
import Modal from 'react-native-modal';

// https://github.com/jacklam718/react-native-modals
interface Props {

}

interface State {
    visible: boolean,
    dialogProps: any
}

export default class DialogComponent extends React.Component<Props, State> {
    childrenComponent: any;

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            dialogProps: {}
        }
    }

    showDialogComponent = () => {
        this.setState({visible: true})
    };

    hideDialogComponent = () => {
        this.setState({visible: false})
    };

    setDialogProps = (dialogProps) => {
        this.setState({dialogProps})
    };

    renderChildren = (child = <View/>) => {
        this.childrenComponent = child;
    };

    render() {
        const {dialogProps} = this.state;

        return (
            <Modal
                onTouchOutside={this.hideDialogComponent}
                onHardwareBackPress={this.hideDialogComponent}
                onBackdropPress={this.hideDialogComponent}
                onBackButtonPress={this.hideDialogComponent}
                isVisible={this.state.visible}
                swipeDirection={['up', 'down']} // can be string or an array
                swipeThreshold={200} // default 100
                onSwipeCancel={this.hideDialogComponent}
                deviceWidth={1}
                deviceHeight={1}
                animationIn="fadeIn"
                animationOut="fadeOut"
                backdropOpacity={0.9}
                coverScreen={false}
                backdropColor="white"
                {...this.state.dialogProps}
            >
                {this.childrenComponent
                    ? <View style={[
                        {
                            flex: 1,
                            backgroundColor: 'rgba(0,0,0,0.6)'
                        },
                        dialogProps.containerStyle ? dialogProps.containerStyle : {}]}>
                        <StatusBar
                            backgroundColor="rgba(0, 0, 0, 1)"
                            barStyle={"light-content"}/>
                        {this.childrenComponent}
                    </View>
                    : <View/>}
            </Modal>
        )
    }
};
