import React from 'react';
import {Provider} from "react-redux";
import {
    StyleSheet,
    View,
} from 'react-native';
import store from './src/reducers/reducers';
import AppNavigator from './src/navigation/RootNavigation';


const App = () => {
    return (
        <Provider store={store}>
            <AppNavigator/>
        </Provider>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EEEEEE',
    },
});

export default App;
